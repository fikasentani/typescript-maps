import { EndLocation } from './EndLocation';
import { StartLocation } from './StartLocation';
import { CustomMap } from './CustomMap';

const endLocation = new EndLocation();
const startLocation = new StartLocation();
const customMap = new CustomMap('map');

customMap.addMarker(startLocation);
customMap.addMarker(endLocation);